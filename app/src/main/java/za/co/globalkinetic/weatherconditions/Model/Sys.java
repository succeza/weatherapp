package za.co.globalkinetic.weatherconditions.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 4/14/2017.
 */
public class Sys {

    @SerializedName("country")
    private String country;

    @SerializedName("sunrise")
    private double sunnrise;

    @SerializedName("sunset")
    private double sunset;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getSunnrise() {
        return sunnrise;
    }

    public void setSunnrise(double sunnrise) {
        this.sunnrise = sunnrise;
    }

    public double getSunset() {
        return sunset;
    }

    public void setSunset(double sunset) {
        this.sunset = sunset;
    }
}
