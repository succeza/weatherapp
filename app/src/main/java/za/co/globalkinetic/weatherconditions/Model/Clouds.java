package za.co.globalkinetic.weatherconditions.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 4/14/2017.
 */
public class Clouds {


    @SerializedName("all")
    private double all;

    public double getAll() {
        return all;
    }

    public void setAll(double all) {
        this.all = all;
    }
}
