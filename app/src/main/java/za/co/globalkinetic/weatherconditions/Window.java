package za.co.globalkinetic.weatherconditions;


import android.Manifest;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import za.co.globalkinetic.weatherconditions.Location.LocationTracker;
import za.co.globalkinetic.weatherconditions.Location.tDate;
import za.co.globalkinetic.weatherconditions.Model.Results;
import za.co.globalkinetic.weatherconditions.Rest.ApiInterface;
import za.co.globalkinetic.weatherconditions.Rest.Apilient;


/**
 * A simple {@link Fragment} subclass.
 */
public class Window extends android.app.Fragment{


    LocationManager locManager;
    Criteria criteria;
    String provider;
    private TextView tv_loc,tv_time,tv_temp,tv_desc;
    private ImageView img_icon,img_degree;

    private static final String API_KEY ="9e996056dc0f93fd50841322165851d1";
    private double temp;
    private String longitude;
    private String latitude;
    private String time="";
    private String des;
    private String currentLocation;
    AlertDialog progress;
    ActionBar action;
    public Window() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View v= inflater.inflate(R.layout.fragment_window, container, false);
        if (API_KEY.isEmpty()) {
            ShowToast("Please obtain your api key");
        }
        action=getActivity().getActionBar();
        initViews(v);
        //checkKocation();
        locationCheck();
        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    private void initViews(View v){

          tv_loc=(TextView)v.findViewById(R.id.tv_loc);
        tv_time=(TextView)v.findViewById(R.id.tv_time);
        tv_temp=(TextView)v.findViewById(R.id.tv_temp);
        img_icon=(ImageView)v.findViewById(R.id.img_icon);
        tv_desc=(TextView)v.findViewById(R.id.tv_descr);
        img_degree=(ImageView)v.findViewById(R.id.img_degree);
        progress = new SpotsDialog(getActivity(), R.style.Custom);

    }
    private void locationCheck() {
        locManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (!locManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            createLocationAlertDialog().show();
        } else {
            criteria = new Criteria();
            provider = locManager.getBestProvider(criteria, false);
            checkKocation();

        }
        if(!checkPermission(Manifest.permission.ACCESS_FINE_LOCATION) && !checkPermission(Manifest.permission.ACCESS_COARSE_LOCATION))
        {
            ShowToast("Permissions not provided");
        }
    }
    //------Creates a toast-----------------
    private void ShowToast(String message) {
        View layout = LayoutInflater.from(getActivity().getBaseContext()).inflate(R.layout.custom_toast, null);
        TextView tv_message = (TextView) layout.findViewById(R.id.tv_message);
        tv_message.setText(message);
        Toast toast = new Toast(getActivity().getBaseContext());
        toast.setView(layout);
        toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER, 5, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();
    }
    //-------creates an AlertDialog--------------------
    private AlertDialog.Builder createLocationAlertDialog() {
        AlertDialog.Builder dlg = new AlertDialog.Builder(getActivity());
        dlg.setTitle("Location Services Disabled!");
        dlg.setMessage("Do you want to enable your location services?");
        dlg.setIcon(R.mipmap.weather_man);
        dlg.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        });
        dlg.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        return dlg;
    }
    @Override
    public void onResume() {
        super.onResume();

    }
    public void checkKocation(){
        progress.show();
        LocationTracker tracker=new LocationTracker(getActivity());
        //------get the lat and lon of the location-----
        latitude=String.valueOf(tracker.getLatitude());
        longitude=String.valueOf(tracker.getLongitude());

        //------Get current time-------


        Operate();
    }
    public void Operate() {
        progress.show();
        if (!isNetworkAvailable()) {
            progress.dismiss();
            ShowToast("No internet connection!!! Please connect to the internet first");
        } else {
            ApiInterface api = Apilient.getApiService();
            //--------Calling json--------------
            Call<Results> call = api.getResults(latitude, longitude, API_KEY);

            call.enqueue(new Callback<Results>() {
                @Override
                public void onResponse(Call<Results> call, Response<Results> response) {
                    try {

                        if (response.isSuccessful()) {
                            progress.dismiss();
                            //----Date
                            tDate today = new tDate();
                            time = String.valueOf(today.getCurrentTime());
                            //----Retrieve the downloaded data-----
                            currentLocation = response.body().getName();
                            temp = response.body().getMain().getTemp();
                            des = response.body().getWeather().get(0).getDescription();
                            printOut(currentLocation, temp, des);
                            //-----Invoke the validate method
                            validate(des);
                            //------
                            tv_time.setText(time);

                        } else {
                            progress.dismiss();
                            ShowToast("not  downloading");
                        }
                    } catch (Exception ee) {
                        Log.d("failed", ee.getLocalizedMessage());
                        Toast.makeText(getActivity(), ee.getMessage().toString(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Results> call, Throwable t) {
                    progress.dismiss();
                    ShowToast("  On failure" + t.toString());
                }
            });
        }
    }
    private boolean checkPermission(String permission) {
        return ContextCompat.checkSelfPermission(getActivity(),
                permission) == PackageManager.PERMISSION_GRANTED;
    }
    private void validate(String des){
        if(des.equals("clear sky")){
            img_icon.setImageResource(R.mipmap.sun);
        }else if(des.equals("overcast clouds")){
            img_icon.setImageResource(R.mipmap.overcast_ion);
        }else if(des.equals("rainy")){
            img_icon.setImageResource(R.mipmap.rainy_icon);
        }else if(des.equalsIgnoreCase("windy")){
            img_icon.setImageResource(R.mipmap.windy);
        }


    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem mnu=menu.add(0,R.id.action_refresh,3, "Refresh");
        mnu.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //Switch decision structure
        switch (item.getItemId()) {
            case R.id.action_refresh:
                //-----Clears the current weather when refreshing-----
                tv_loc.setText("");
                tv_temp.setText("");
                tv_desc.setText("");
                tv_time.setText("");
                img_degree.setVisibility(View.INVISIBLE);
                checkKocation();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    //-----Tests for the availability of the internet---------
    public boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager)  getActivity().
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
        {
            if(networkInfo.getType() == ConnectivityManager.TYPE_MOBILE ||
                networkInfo.getType() == ConnectivityManager.TYPE_WIFI){
            return true;
        }
        }
        return false;
    }
    private void printOut(String current,double temparature,String description){

        tv_loc.setText(current);
        DecimalFormat f =new DecimalFormat("#");
        tv_temp.setText(String.valueOf(f.format(temparature)));
        img_degree.setVisibility(View.VISIBLE);
        description=description.substring(0,1).toUpperCase()+description.substring(1).toLowerCase();
        tv_desc.setText(description);

    }

}
