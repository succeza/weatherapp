package za.co.globalkinetic.weatherconditions;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.location.Criteria;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Display;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends Activity {

    LocationManager locManager;
    Criteria criteria;
    String provider;
    private TextView tv_loc, tv_time, tv_temp;
    private ImageView img_icon;
    private Button btn_refresh;

    private static final String API_KEY = "9e996056dc0f93fd50841322165851d1";
    private String location;
    private String longitude;
    private String latitude;
    private String time;
    private String currentLocation;

    AlertDialog progress;
  ActionBar action;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fragmanager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmanager.beginTransaction();
        WindowManager wm = getWindowManager();
        Display d = wm.getDefaultDisplay();

        Window window1 = new Window();


        fragmentTransaction.replace(R.id.fragment, window1);
        fragmentTransaction.commit();
    }

}
