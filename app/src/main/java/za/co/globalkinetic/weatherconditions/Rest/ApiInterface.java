package za.co.globalkinetic.weatherconditions.Rest;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import za.co.globalkinetic.weatherconditions.Model.Results;

/**
 * Created by user on 4/13/2017.
 */
public interface ApiInterface {

     @GET("weather?")
    Call<Results> getResults(@Query("lat")String latitude,
                             @Query("lon")String longitude,
                             @Query("appid")String appid);

}
