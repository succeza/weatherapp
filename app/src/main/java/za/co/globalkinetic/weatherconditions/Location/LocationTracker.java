package za.co.globalkinetic.weatherconditions.Location;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

/**
 * Created by user on 4/14/2017.
 */
public class LocationTracker extends Service implements LocationListener {

    //---Gets the lass name------
    private final String TAG = LocationTracker.class.getName();
    private final Context CContext;
    boolean isGPSEnabled = false;
    boolean isNetworkEnabled = false;
    Location location;
    double latitude;
    double longitude;
    //---------Declaring a location manager----
    protected LocationManager locationManager;
    //-----Store location manager-----
    private String provider_info;
    //------
    private static final long DISTANCE = 10;
    private static final long TIME1 = 1000 * 60 * 1;

    public LocationTracker(Context context) {
        this.CContext = context;
        getLocaton();

    }

    public void getLocaton() {
        try {
            locationManager = (LocationManager) CContext.getSystemService(Context.LOCATION_SERVICE);

            //----Getting GPS status-----
            isGPSEnabled = locationManager.isProviderEnabled(locationManager.GPS_PROVIDER);

            //---Getting network status-----
            isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);


            //------ Try to get location is gps is enabled------
            if (isGPSEnabled) {
                this.isGPSEnabled = true;

                Log.d(TAG, "Using GPS services");

                provider_info = LocationManager.GPS_PROVIDER;

                //----try to get location using network-------
            } else if (isNetworkEnabled) {
                this.isNetworkEnabled = true;

                Log.d(TAG, "Using network services");

                provider_info = LocationManager.NETWORK_PROVIDER;
            }

            if (!provider_info.isEmpty()) {
                if (ActivityCompat.checkSelfPermission(CContext, Manifest.permission.ACCESS_FINE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                        (CContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                locationManager.requestLocationUpdates(provider_info, DISTANCE, TIME1, this
                );
                if(locationManager!=null){
                    location=locationManager.getLastKnownLocation(provider_info);
                    updateCoordinates();

                }
            }

        }catch (Exception e){
            Log.d(TAG,"ant connect to manager");
        }

    }
    //-----Tries to update latitude and longitude-------
    public void updateCoordinates(){
        if(location!=null){
            latitude=location.getLatitude();
            longitude=location.getLongitude();
        }
    }
    //-----Tries to get the longitude-------
    public double getLongitude() {
        if (location != null) {

            longitude = location.getLongitude();
        }
        return longitude;
    }
    //-----Tries to get the latitude--------
    public double getLatitude() {
        if (location != null) {

            latitude = location.getLatitude();
        }
        return latitude;
    }
    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
