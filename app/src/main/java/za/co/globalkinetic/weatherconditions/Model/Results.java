package za.co.globalkinetic.weatherconditions.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 4/13/2017.
 */
public class Results {

    private double dt;
    private Main main;
    private List<Weather> weather=new ArrayList<Weather>();
    private Clouds clouds;
    private Rain rain;
    private Sys sys;
    private String dtText;
    @SerializedName("id")
    private double id;
    @SerializedName("name")
    private String name;
    @SerializedName("cod")
    private double cod;

    public Clouds getClouds() {
        return clouds;
    }

    public void setClouds(Clouds clouds) {
        this.clouds = clouds;
    }

    public Rain getRain() {
        return rain;
    }

    public void setRain(Rain rain) {
        this.rain = rain;
    }

    public Sys getSys() {
        return sys;
    }

    public void setSys(Sys sys) {
        this.sys = sys;
    }

    public String getDtText() {
        return dtText;
    }

    public void setDtText(String dtText) {
        this.dtText = dtText;
    }

    public double getDt() {

        return dt;
    }

    public void setDt(double dt) {
        this.dt = dt;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
    }

       public double getId() {
        return id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public double getCod() {
        return cod;
    }

    public void setCod(double cod) {
        this.cod = cod;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
