package za.co.globalkinetic.weatherconditions.Rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by user on 4/13/2017.
 */
public class Apilient {

    public static final String BASE_URL1 = "http://api.openweathermap.org/data/2.5/";
    private static Retrofit retrofit = null;

    public static Retrofit getClient() {

        //OkHttpClient cliennt=new OkHttpClient().Builder.class
        return new Retrofit.Builder()
                .baseUrl(BASE_URL1)
                .addConverterFactory(GsonConverterFactory.create())
                .build();



    }
    public static ApiInterface getApiService(){
        return getClient().create(ApiInterface.class);

    }

}
